import React, { useState } from "react";
import {
  StyleSheet,
  SafeAreaView,
  View,
  Dimensions,
  ScrollView,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { StatusBar } from "expo-status-bar";
import * as Location from "expo-location";
import { AnimatedCircularProgress } from "react-native-circular-progress";
// import * as Font from "expo-font";
import Constants from "expo-constants";
import { RFPercentage } from "react-native-responsive-fontsize";
import { CustomText } from "./components/helpers/custom_text";
//? ================  COMPONENTS  ====================
import GooglePlacesInput from "./components/autocomplete";
import Buttons from "./components/buttons";
import SectionHeader from "./components/section_header";
import DataContainer from "./components/data_container";
import HoursContainer from "./components/hours_container";
import ForecastContainer from "./components/forecast_container";
import FavoritesContainer from "./components/favorites_container";
import Loader from "./components/loader";
const { width, height } = Dimensions.get("window");
import { OWP_API_KEY } from "./config";
import { AntDesign, EvilIcons } from "@expo/vector-icons";

export default function App() {
  const [displayInput, setDisplayInput] = React.useState(false);
  const [favourites, setFavourites] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [favouriteNames, setFavouriteNames] = useState([]);
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [state, setState] = React.useState({
    loaded: true,
    //=====================
    name: "Barcelona",
    dt: 1588352808,
    wind: 9.3,
    icon: "03d",
    temp: 26.34,
    temp_min: 25,
    temp_max: 27.78,
    humidity: 39,
    sunset: 1588445381,
    latitude: 0,
    longitude: 0,
    timezone: "Europe/Madrid",
    //=====================
    hourly: [],
    forecast: [],
    get_favorites: true,
  });

  React.useEffect(() => {
    const loadFonts = async () => {
      //await AsyncStorage.removeItem('favorites')
      try {
        // await Font.loadAsync({
        //   Montserrat: require("./assets/fonts/Montserrat-Regular.ttf"),
        // });
        (async () => {
          let { status } = await Location.requestForegroundPermissionsAsync();
          if (status !== "granted") {
            setErrorMsg("Permission to access location was denied");
            return;
          }
          await getCurrentPosition();
        })();
        await getFavourites();
      } catch (error) {
        alert("Error from loadFonts");
      }
    };
    loadFonts();
  }, []);
  //?======================================================
  //?==============  get current position  ================
  //?======================================================
  const getCurrentPosition = async () => {
    let location = await Location.getCurrentPositionAsync({});
    const { latitude, longitude } = location.coords;
    getWeather(latitude, longitude);
    setLocation(location);
  };
  //?======================================================
  //?===================  get weather  ====================
  //?======================================================
  const getWeather = async (
    latitude,
    longitude,
    toggle = false,
    scroll = false
  ) => {
    try {
      const currentJson = await fetch(
        `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&units=metric&appid=${OWP_API_KEY}`
      );
      const current = await currentJson.json();
      const oneCallJson = await fetch(
        `https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&units=metric&appid=${OWP_API_KEY}`
      );
      const oneCall = await oneCallJson.json();
      const tempHours = [],
        tempForecast = [];
      oneCall.daily.map((ele, idx) =>
        idx < 7 && idx > 0 ? tempForecast.push(ele) : null
      );
      setState({
        ...state,
        name: current.name,
        icon: current.weather[0].icon,
        temp: current.main.temp,
        temp_min: current.main.temp_min,
        temp_max: current.main.temp_max,
        sunset: current.sys.sunset,
        humidity: current.main.humidity,
        wind: current.wind.speed,
        dt: current.dt,
        hourly: oneCall.hourly,
        forecast: [...tempForecast],
        timezone: oneCall.timezone,
        loaded: true,
        latitude: Math.round(latitude * 1000) / 1000,
        longitude: Math.round(longitude * 1000) / 1000,
      });
      toggle ? toggleInput() : null;
      scroll ? scrollToTop() : null;
      setIsLoading(false);
    } catch (error) {
      console.log("error ==>", error);
      alert("error ===>");
    }
  };
  //?======================================================
  //?=================  custom alert  =====================
  //?======================================================
  // const customAlert = (message) => {
  //   return Alert.alert("Something went wrong", message);
  // };
  //?======================================================
  //?===================  get favorites  ==================
  //?======================================================
  const getFavourites = async () => {
    try {
      const tempFavAsync = await AsyncStorage.getItem("favourites");
      if (tempFavAsync === null) {
        await AsyncStorage.setItem("favourites", JSON.stringify([]));
        return;
      } else {
        const parsed = JSON.parse(tempFavAsync);
        if (parsed.length === 0) return;
        const urls = [];
        parsed.forEach((ele) =>
          urls.push({
            uri: `https://api.openweathermap.org/data/2.5/weather?lat=${ele.latitude}&lon=${ele.longitude}&units=metric&appid=${OWP_API_KEY}`,
            ...ele,
          })
        );
        const tempFav = [];
        const names = [];
        await Promise.all(
          urls.map(async ({ uri, name, latitude, longitude }) => {
            names.push(name);
            const r = await fetch(uri);
            const data = await r.json();
            tempFav.push({
              name,
              icon: data.weather[0].icon,
              temp: Math.round(data.main.temp),
              latitude,
              longitude,
            });
          })
        );
        setFavourites(tempFav);
        setFavouriteNames(names);
      }
    } catch (error) {
      customAlert("Internal server error");
    }
  };
  //?======================================================
  //?=================  create favorite  ==================
  //?======================================================
  const createFavorite = async () => {
    try {
      const tempFavAsync = await AsyncStorage.getItem("favourites");
      const tempFavNames = [...favouriteNames];

      if (tempFavAsync === null) {
        await AsyncStorage.setItem("favourites", JSON.stringify([]));
        return;
      } else {
        if (favouriteNames.includes(state.name)) return;
        const parsed = JSON.parse(tempFavAsync);
        if (parsed.length > 2) return;
        parsed.push({
          latitude: state.latitude,
          longitude: state.longitude,
          name: state.name,
        });
        await AsyncStorage.setItem("favourites", JSON.stringify(parsed));
        const url = `https://api.openweathermap.org/data/2.5/weather?lat=${state.latitude}&lon=${state.longitude}&units=metric&appid=${OWP_API_KEY}`;
        const r = await fetch(url);
        const data = await r.json();
        const tempFav = [...favourites];
        tempFav.push({
          name: state.name,
          icon: data.weather[0].icon,
          temp: Math.round(data.main.temp),
        });
        tempFavNames.push(state.name);
        setFavouriteNames(tempFavNames);
        setFavourites(tempFav);
      }
    } catch (error) {
      customAlert("Internal server error");
    }
  };
  //?======================================================
  //?=================  delete favorite  ==================
  //?======================================================
  const deleteFavorite = async (name) => {
    try {
      const tempFavAsync = await AsyncStorage.getItem("favourites");
      const tempFavNames = [...favouriteNames];
      const tempFav = [...favourites];
      if (tempFavAsync === null) {
        await AsyncStorage.setItem("favourites", JSON.stringify([]));
        return;
      } else {
        if (!tempFavNames.includes(name)) return;
        const parsed = JSON.parse(tempFavAsync);
        const parsedIndex = parsed.findIndex((el) => el.name === name);
        if (parsed.length === 0 || parsedIndex === -1) return;
        const tempFavNamesIndex = tempFavNames.findIndex((el) => el === name);
        if (tempFavNamesIndex === -1) return;
        const tempFavIndex = tempFav.findIndex((el) => el.name === name);
        if (tempFavIndex === -1) return;
        parsed.splice(parsedIndex, 1);
        tempFavNames.splice(tempFavNamesIndex, 1);
        tempFav.splice(tempFavIndex, 1);
        await AsyncStorage.setItem("favourites", JSON.stringify(parsed));
        setFavouriteNames(tempFavNames);
        setFavourites(tempFav);
      }
    } catch (error) {
      console.log("error ====>", error);
      customAlert("Internal server error");
    }
  };

  const toggleInput = () => {
    setDisplayInput(!displayInput);
    if (!displayInput) {
      scrollViewRef.current.scrollTo({ y: height / 2 });
    } else {
      scrollViewRef.current.scrollTo({ y: 0 });
    }
  };
  //?======================================================
  //?====================  refs  ==========================
  //?======================================================
  const scrollViewRef = React.useRef(null);

  const scrollToBottom = () =>
    scrollViewRef.current.scrollTo({ y: height * 2.5 });

  const scrollToTop = () => scrollViewRef.current.scrollTo({ y: 0 });
  //?======================================================
  //?====================  render  ========================
  //?======================================================
  return (
    <>
      <StatusBar style="light" backgroundColor="rgba(0,0,0,0.75)" />
      {isLoading ? (
        <Loader />
      ) : (
        <SafeAreaView style={styles.container}>
          {state.loaded ? (
            <ScrollView
              keyboardShouldPersistTaps="always"
              style={styles.scroll_container}
              ref={scrollViewRef}
            >
              {/*****************************************/}
              <View style={[styles.header, styles.center]}>
                <CustomText style={styles.header_text}>{state.name}</CustomText>
              </View>
              {/*****************************************/}
              <View style={[styles.section1, styles.center]}>
                <AnimatedCircularProgress
                  size={height / 3}
                  width={1}
                  fill={100}
                  tintColor="#39CCCC"
                  onAnimationComplete={() => console.log("onAnimationComplete")}
                  backgroundColor="#ddd"
                  rotation={210}
                  lineCap={"square"}
                  arcSweepAngle={300}
                >
                  {() => (
                    <CustomText style={styles.degrees_text}>
                      {Math.round(state.temp)}
                    </CustomText>
                  )}
                </AnimatedCircularProgress>
                <AntDesign
                  onPress={() =>
                    favouriteNames.includes(state.name)
                      ? deleteFavorite(state.name)
                      : createFavorite()
                  }
                  name={favouriteNames.includes(state.name) ? "minus" : "plus"}
                  size={60}
                  color="#39CCCC"
                  style={{ position: "absolute", bottom: height / 200 }}
                />
              </View>
              {/*****************************************/}
              <Buttons
                scrollToBottom={scrollToBottom}
                toggleInput={toggleInput}
                getCurrentPosition={getCurrentPosition}
              />
              <View>
                {displayInput ? (
                  <GooglePlacesInput
                    toggleInput={toggleInput}
                    getWeather={getWeather}
                  />
                ) : null}
              </View>
              <DataContainer {...state} />
              <SectionHeader dt={state.dt} />
              <HoursContainer hourly={state.hourly} timezone={state.timezone} />
              <SectionHeader text={"Forecast"} />
              <ForecastContainer forecast={state.forecast} />
              <SectionHeader text={"Favorites"} />
              <FavoritesContainer
                favourites={favourites}
                deleteFavorite={deleteFavorite}
                getWeather={getWeather}
              />
              <View style={[styles.footer, styles.center]}>
                <EvilIcons
                  name="arrow-up"
                  size={70}
                  color="#39CCCC"
                  onPress={() => scrollToTop()}
                />
              </View>
            </ScrollView>
          ) : (
            <Loader />
          )}
        </SafeAreaView>
      )}
    </>
  );
}

const styles = StyleSheet.create({
  //? ==========  MAIN CONTAINER  ==========
  container: {
    width,
    height,
    marginTop: Constants.statusBarHeight,
  },
  scroll_container: {
    width,
    height,
    backgroundColor: "rgba(0,0,0,0.75)",
  },
  //? ==========  GENERAL  ==========
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  evenly: {
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  between: {
    alignItems: "center",
    justifyContent: "space-between",
  },
  //? ==========  HEADER  ==========
  header: {
    width,
    height: (height / 8) * 1,
  },
  header_text: {
    color: "#CCCCCC",
    fontSize: RFPercentage(5),
  },
  //? ==========  SECTION1  ==========
  section1: {
    width,
    height: (height / 8) * 3,
    marginTop: height / 22,
  },
  degrees_text: {
    color: "#CCCCCC",
    marginBottom: 5,
    fontSize: RFPercentage(15),
    fontWeight: "100",
  },
  //? ==========  FOOTER  ==========
  footer: {
    width,
    height: height / 9,
  },
});

const s = StyleSheet.create({
  modal__header: {
    position: "absolute",
    top: 20,
    right: 20,
    zIndex: 9000,
    alignItems: "center",
    justifyContent: "center",
    width: 25,
    height: 25,
    backgroundColor: "rgba(0, 0, 0, 0.8)",
    borderRadius: 4,
  },

  content: {
    padding: 15,
    position: "absolute",
    width,
    height,
    backgroundColor: "red",
  },

  content__heading: {
    marginBottom: 2,
    fontSize: 24,
    fontWeight: "600",
    color: "#333",
  },

  content__subheading: {
    marginBottom: 20,
    fontSize: 16,
    color: "#ccc",
  },

  content__paragraph: {
    fontSize: 15,
    fontWeight: "200",
    lineHeight: 22,
    color: "#666",
  },
});
