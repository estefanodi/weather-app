# Weather App

React Native weather app, it will get the current coordinates through expo-location. The coordinate are used to make an api call from which it get the weather data. The App will display:

* current weather

* hourly weather

* forecast

From an input you can select a city different from the current one and eventually display the weather.

You can cities in a favourite area at the bottom of the app.

### Apis used:

* Open Weather Map

* Google geocode

<br>
<br>
<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1620024613/WhatsApp_Image_2021-05-03_at_08.46.35_google-pixel4xl-justblack-portrait.png" alt="splash screen" width='250px'>
<br>
<br>
<br>
<br>
<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1619992255/WhatsApp_Image_2021-05-02_at_23.21.41_2__google-pixel4xl-justblack-portrait.png" alt="splash screen" width='250px'>
<br>
<br>
<br>
<br>
<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1619992255/WhatsApp_Image_2021-05-02_at_23.21.41_1__google-pixel4xl-justblack-portrait.png" alt="splash screen" width='250px'>
<br>
<br>
<br>
<br>
<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1619992255/WhatsApp_Image_2021-05-02_at_23.29.29_google-pixel4xl-justblack-portrait.png" alt="splash screen" width='250px'>

<div>

[<img src="https://res.cloudinary.com/estefanodi2009/image/upload/v1629320690/en_badge_web_generic.png" alt="splash screen" 
width='300px'>](https://play.google.com/store/apps/details?id=com.stefano.wapp)

</div>





