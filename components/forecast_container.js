import React from "react";
import { StyleSheet, Dimensions, View } from "react-native";
import moment from "moment";
import { RFPercentage } from "react-native-responsive-fontsize";

import { CustomText } from "./helpers/custom_text";
import { WeatherIcon } from "./helpers/icons";

const { width, height } = Dimensions.get("window");

export default function DataContainer({ forecast = [] }) {
  return (
    <View style={styles.container}>
      {forecast.map((ele, i) => {
        return (
          <View style={styles.forecast_in} key={i}>
            <View style={[styles.forecast_in_title, styles.center]}>
              <CustomText style={styles.forecast_in_text}>
                {moment(ele.dt * 1000).format("dddd")}
              </CustomText>
            </View>

            <View style={[styles.forecast_in_icon, styles.center]}>
              {WeatherIcon(ele.weather[0].icon, 7, "#CCCCCC")}
            </View>

            <View style={[styles.forecast_in_temp, styles.center]}>
              <CustomText style={styles.forecast_in_text2}>
                {Math.round(ele.temp.day)}°
              </CustomText>
            </View>
          </View>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  //? ==========  GENERAL  ==========
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  evenly: {
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  between: {
    alignItems: "center",
    justifyContent: "space-between",
  },
  //? ==========  MAIN CONTAINER  ==========
  container: {
    width,
    height: height / 1.5,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
  },
  //? ==========  CONTENT  ==========
  forecast_in: {
    width: width / 3.5,
    height: "45%",
    marginTop: width / 27,
    flexDirection: "column",
    backgroundColor: "rgba(255,255,255,0.1)",
  },
  forecast_in_text: {
    color: "#39CCCC",
    fontSize: RFPercentage(2.5),
  },
  forecast_in_title: {
    height: "20%",
  },
  forecast_in_icon: {
    height: "55%",
  },
  forecast_in_temp: {
    height: "25%",
    flexDirection: "row",
  },
  forecast_in_text2: {
    color: "#39CCCC",
    fontSize: RFPercentage(5),
  },
});
