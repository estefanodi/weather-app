import React from "react";
import Constants from "expo-constants";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, View, Dimensions } from "react-native";
import {
  // BallIndicator,
  // BarIndicator,
  DotIndicator,
  // MaterialIndicator,
  // PacmanIndicator,
  // PulseIndicator,
  // SkypeIndicator,
  // UIActivityIndicator,
  // WaveIndicator,
} from "react-native-indicators";
const { width, height } = Dimensions.get("window");

export default function Loader() {
  return (
    <View style={styles.container}>
      <StatusBar style="light" backgroundColor="transparent" />
      <DotIndicator color="#39CCCC" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width,
    height: height + Constants.statusBarHeight,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0,0,0,0.75)",
  },
});
