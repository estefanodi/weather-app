export const list = [
  {
    dt_txt: "2020-05-02 15:00:00",
    weather: [
      {
        icon: "04d",
        main: "Clouds",
        description: "broken clouds",
      },
    ],
    main: {
      temp: 22.21,
      temp_min: 21.02,
      temp_max: 22.21,
    },
    wind: {
      speed: 9.3
    }
  },
  {
    dt_txt: "2020-05-02 18:00:00",
    weather: [
      {
        main: "Clouds",
        description: "broken clouds",
        icon: "04d",
      },
    ],
    main: {
      temp: 20.22,
      temp_min: 19.36,
      temp_max: 20.22,
    },
    wind: {
      speed: 9.3
    }
  },
  {
    dt_txt: "2020-05-02 21:00:00",
    weather: [
      {
        main: "Clouds",
        description: "broken clouds",
        icon: "04n",
      },
    ],
    main: {
      temp: 18.73,
      temp_min: 18.41,
      temp_max: 18.73,
    },
    wind: {
      speed: 9.3
    }
  },
  {
    dt_txt: "2020-05-03 00:00:00",
    weather: [
      {
        main: "Clouds",
        description: "broken clouds",
        icon: "04n",
      },
    ],
    main: {
      temp: 18.13,
      temp_min: 18.09,
      temp_max: 18.13,
    },
    wind: {
      speed: 9.3
    }
  },
  {
    dt_txt: "2020-05-03 03:00:00",
    weather: [
      {
        main: "Clouds",
        description: "overcast clouds",
        icon: "04n",
      },
    ],
    main: {
      temp: 17.67,
      temp_min: 17.67,
      temp_max: 17.67,
    },
    wind: {
      speed: 9.3
    }
  },
  {
    dt_txt: "2020-05-03 06:00:00",
    weather: [
      {
        main: "Clouds",
        description: "overcast clouds",
        icon: "04n",
      },
    ],
    main: {
      temp: 17.82,
      temp_min: 17.82,
      temp_max: 17.82,
    },
    wind: {
      speed: 9.3
    }
  },
];
