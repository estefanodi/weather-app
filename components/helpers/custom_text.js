import * as React from "react";
import { Text } from "react-native";
import { RFPercentage } from "react-native-responsive-fontsize";

export const CustomText = (props) => {
  return (
    <Text
      {...props}
      style={[
        props.style,
        // { fontFamily: "Montserrat" },
      ]}
    />
  );
};
