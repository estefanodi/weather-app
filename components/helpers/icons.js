import React from "react";
import {
  Fontisto,
  FontAwesome,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import { RFPercentage } from "react-native-responsive-fontsize";

const Icons = {
  "01d": { icon: "day-sunny", type: "Fontisto" },
  "01n": { icon: "moon-o", type: "FontAwesome" },
  "02d": { icon: "day-cloudy", type: "Fontisto" },
  "02n": { icon: "night-alt-cloudy", type: "Fontisto" },
  "03d": { icon: "ios-cloud-outline", type: "Ionicons" },
  "03n": { icon: "night-alt-cloudy", type: "Fontisto" },
  "04d": { icon: "cloudy-gusts", type: "Fontisto" },
  "04n": { icon: "cloudy-gusts", type: "Fontisto" },
  "09n": { icon: "umbrella", type: "Fontisto" },
  "09d": { icon: "umbrella", type: "Fontisto" },
  "10d": { icon: "day-rain", type: "Fontisto" },
  "10n": { icon: "night-alt-rain", type: "Fontisto" },
  "11d": { icon: "weather-lightning", type: "MaterialCommunityIcons" },
  "11n": { icon: "weather-lightning", type: "MaterialCommunityIcons" },
  "12d": { icon: "ios-snow", type: "Ionicons" },
  "12n": { icon: "ios-snow", type: "Ionicons" },
  "50d": { icon: "fog", type: "Fontisto" },
  "50n": { icon: "fog", type: "Fontisto" },
};

export const WeatherIcon = (icon, size, color) => {
  switch (Icons[icon].type) {
    case "Fontisto":
      return (
        <Fontisto
          name={Icons[icon].icon}
          size={RFPercentage(size)}
          color={color}
        />
      );
    case "FontAwesome":
      return (
        <FontAwesome
          name={Icons[icon].icon}
          size={RFPercentage(size)}
          color={color}
        />
      );
    case "Ionicons":
      return (
        <Ionicons
          name={Icons[icon].icon}
          size={RFPercentage(size)}
          color={color}
        />
      );
    case "MaterialCommunityIcons":
      return (
        <MaterialCommunityIcons
          name={Icons[icon].icon}
          size={RFPercentage(size)}
          color={color}
        />
      );
  }
};
