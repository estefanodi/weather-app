import React from "react";
import { StyleSheet, Dimensions, View } from "react-native";
import moment from "moment-timezone";
import { RFPercentage } from "react-native-responsive-fontsize";
import { Feather, AntDesign } from "@expo/vector-icons";

import { CustomText } from "./helpers/custom_text";
import { WeatherIcon } from "./helpers/icons";

const { width, height } = Dimensions.get("window");

export default function DataContainer({
  temp_min,
  temp_max,
  humidity,
  wind,
  sunset,
  timezone,
}) {
  return (
    <>
      <View style={[styles.data_container]}>
        <View style={[styles.data_container_in, styles.center]}>
          {WeatherIcon("03n", 5, "#39CCCC")}
        </View>
        <View style={[styles.data_container_in, styles.center]}>
          <AntDesign name="arrowdown" size={RFPercentage(4)} color="#39CCCC" />
          <CustomText style={styles.data_container_text}>
            {Math.round(temp_min)}°
          </CustomText>
        </View>
        <View style={[styles.data_container_in, styles.center]}>
          <AntDesign name="arrowup" size={RFPercentage(4)} color="#39CCCC" />
          <CustomText style={styles.data_container_text}>
            {Math.round(temp_max)}°
          </CustomText>
        </View>
      </View>

      <View style={[styles.data_container2]}>
        <View style={[styles.data_container_in, styles.center]}>
          <CustomText style={styles.data_container_text}>
            {Math.round(wind)}mph
          </CustomText>
          <Feather name="wind" size={RFPercentage(3)} color="#39CCCC" />
        </View>
        <View style={[styles.data_container_in, styles.center]}>
          <CustomText style={styles.data_container_text}>
            {humidity}%
          </CustomText>
          <Feather name="droplet" size={RFPercentage(3)} color="#39CCCC" />
        </View>
        <View style={[styles.data_container_in, styles.evenly]}>
          <Feather name="sunset" size={RFPercentage(3)} color="#39CCCC" />
          <CustomText style={styles.data_container_text}>
            {moment(sunset * 1000)
              .tz(timezone)
              .format("H:mm")}
          </CustomText>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  evenly: {
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  between: {
    alignItems: "center",
    justifyContent: "space-between",
  },
  data_container: {
    width,
    height: (height / 8) * 1.1,
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-evenly",
    paddingBottom: height / 100,
  },
  data_container2: {
    width,
    height: (height / 8) * 1.1,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "space-evenly",
    paddingTop: height / 100,
  },
  data_container_in: {
    width: width / 3.4,
    height: "80%",
    backgroundColor: "rgba(255,255,255,0.1)",
    flexDirection: "row",
  },
  data_container_text: {
    color: "#CCCCCC",
    fontSize: RFPercentage(2.6),
  },
});
