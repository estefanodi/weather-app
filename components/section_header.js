import React from "react";
import { StyleSheet, Dimensions, View } from "react-native";
import moment from "moment";

import { CustomText } from "./helpers/custom_text";

import { RFPercentage } from "react-native-responsive-fontsize";
const { width, height } = Dimensions.get("window");

export default function SectionHeader({ dt, text = null }) {
  return (
    <View style={[styles.container, styles.center]}>
      <CustomText style={styles.header_text}>
        {dt ? moment(dt * 1000).format("dddd    D") : text}
      </CustomText>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width,
    height: (height / 8) * 1.5,
  },
  header_text: {
    color: "#CCCCCC",
    fontSize: RFPercentage(4),
  },
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
});
