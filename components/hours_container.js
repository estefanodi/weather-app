import React from "react";
import { StyleSheet, Dimensions, View, TouchableOpacity } from "react-native";
import moment from "moment-timezone";
import Steps from "react-native-steps";
import { RFPercentage } from "react-native-responsive-fontsize";
import { Feather, AntDesign } from "@expo/vector-icons";

import { CustomText } from "./helpers/custom_text";
import { WeatherIcon } from "./helpers/icons";

const { width, height } = Dimensions.get("window");

export default function HoursContainer({ hourly = [], timezone }) {
  const [state, setState] = React.useState({
    current: 0,
    labels: [],
    weather: [],
    range: 3,
    options: [
      { opt: "1 hour", idx: 1 },
      { opt: "2 hours", idx: 2 },
      { opt: "3 hours", idx: 3 },
    ],
  });

  const changeOption = (option) => {
    switch (option) {
      case "1 hour":
        return setHourlyWeather(1);
      case "2 hours":
        return setHourlyWeather(2);
      case "3 hours":
        return setHourlyWeather(3);
      default:
        return;
    }
  };

  React.useEffect(() => {
    setHourlyWeather(3);
  }, [hourly]);

  const setHourlyWeather = (range) => {
    const temp = [],
      tempHours = [];
    hourly.forEach((ele, idx) => {
      idx % range === 0 && idx < range * 6
        ? (temp.push(
            moment(ele.dt * 1000)
              .tz(timezone)
              .format("H:mm")
          ),
          tempHours.push(ele))
        : null;
    });
    return setState({
      ...state,
      labels: [...temp],
      weather: [...tempHours],
      range,
    });
  };

  return (
    <>
      <View style={styles.container}>
        <View style={styles.hourly_container_left}>
          <Steps
            configs={configs}
            current={null}
            labels={state.labels}
            count={6}
            direction={"vertical"}
          />
        </View>
        <View style={styles.hourly_container_right}>
          {state.weather.map((ele, idx) => {
            return (
              <View key={idx} style={[styles.hourly_in]}>
                <View style={[styles.hourly_in_1, styles.center]}>
                  {WeatherIcon(ele.weather[0].icon, 3, "#ffffff")}
                </View>
                <View style={[styles.hourly_in_2, styles.center]}>
                  <AntDesign
                    name="arrowup"
                    size={RFPercentage(2)}
                    color="#39CCCC"
                  />
                  <CustomText style={styles.hourly_in_text}>
                    {Math.round(ele.temp)}°
                  </CustomText>
                </View>
                <View style={[styles.hourly_in_3, styles.center]}>
                  <CustomText style={styles.hourly_in_text}>
                    {Math.round(ele.wind_speed)}mph
                  </CustomText>
                  <Feather name="wind" size={RFPercentage(2)} color="#39CCCC" />
                </View>
              </View>
            );
          })}
        </View>
      </View>
      <View style={[styles.buttons, styles.evenly]}>
        {state.options.map((ele, idx) => {
          return (
            <TouchableOpacity
              style={[styles.button, styles.center]}
              key={idx}
              onPress={() => changeOption(ele.opt)}
            >
              <CustomText
                style={[
                  styles.hourly_in_text,
                  { color: state.range === ele.idx ? "#39CCCC" : "#CCCCCC" },
                ]}
              >
                {ele.opt}
              </CustomText>
            </TouchableOpacity>
          );
        })}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  //? ==========  GENERAL  ==========
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  evenly: {
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  between: {
    alignItems: "center",
    justifyContent: "space-between",
  },
  //? ==========  MAIN CONTAINER  ==========
  container: {
    width,
    height: (height / 8) * 4,
    flexDirection: "row",
    paddingHorizontal: width / 24,
  },
  //? ==========  LEFT SIDE  ==========
  hourly_container_left: {
    height: "100%",
    width: width / 3.5,
  },
  //? ==========  RIGHT SIDE  ==========
  hourly_container_right: {
    height: "100%",
    width: width / 1.5,
    flexDirection: "column",
    minHeight: "100%",
  },
  hourly_in: {
    width: "100%",
    height: "16.66%",
    flexDirection: "row",
  },
  hourly_in_1: {
    width: "28%",
    height: "100%",
  },
  hourly_in_2: {
    width: "28%",
    height: "100%",
    flexDirection: "row",
  },
  hourly_in_text: {
    color: "white",
    fontSize: RFPercentage(2),
  },
  hourly_in_3: {
    width: "44%",
    height: "100%",
    flexDirection: "row",
  },
  //? ===========  BUTTONS  ===========
  buttons: {
    width,
    height: height / 12,
    flexDirection: "row",
  },
  button: {
    width: "28%",
    height: "50%",
    backgroundColor: "rgba(255,255,255,0.1)",
  },
});

const configs = {
  stepIndicatorSize: 18,
  currentStepIndicatorSize: 22,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: "#39CCCC",
  stepStrokeWidth: null,
  stepStrokeFinishedColor: "#fe7013",
  stepStrokeUnFinishedColor: "#39CCCC",
  separatorFinishedColor: "#39CCCC",
  separatorUnFinishedColor: "#39CCCC",
  stepIndicatorFinishedColor: "#fe7013",
  stepIndicatorUnFinishedColor: "#39CCCC",
  stepIndicatorCurrentColor: "#39CCCC",
  stepIndicatorLabelFontSize: null,
  currentStepIndicatorLabelFontSize: null,
  stepIndicatorLabelCurrentColor: "transparent",
  stepIndicatorLabelFinishedColor: "#39CCCC",
  stepIndicatorLabelUnFinishedColor: "transparent",
  labelColor: "#39CCCC",
  labelSize: RFPercentage(2.5),
  currentStepLabelColor: "#fe7013",
};
