import React from "react";
import { View, TouchableOpacity, StyleSheet, Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
import { RFPercentage } from "react-native-responsive-fontsize";
import { EvilIcons, Fontisto } from "@expo/vector-icons";

export default function Buttons({
  scrollToBottom,
  toggleInput,
  getCurrentPosition,
}) {
  return (
    <View style={[styles.container, styles.evenly]}>
      <TouchableOpacity
        onPress={() => getCurrentPosition()}
        style={[styles.button, styles.center, { alignItems: "flex-start" }]}
      >
        <EvilIcons name="location" size={RFPercentage(6)} color="#39CCCC" />
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.button, styles.center]}
        onPress={() => scrollToBottom()}
      >
        <Fontisto name="bookmark" size={RFPercentage(4)} color="#39CCCC" />
      </TouchableOpacity>

      <TouchableOpacity
        style={[styles.button, styles.center, { alignItems: "flex-end" }]}
      >
        <EvilIcons
          name="search"
          size={RFPercentage(6)}
          color="#39CCCC"
          onPress={() => toggleInput()}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width,
    height: (height / 8) * 1.2,
    flexDirection: "row",
  },
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  evenly: {
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  button: {
    width: width / 3.6,
    height: "80%",
  },
  button_text: {
    color: "white",
    fontSize: RFPercentage(3),
  },
});
