import React from "react";
import { StyleSheet, Dimensions, View, TouchableOpacity } from "react-native";
import { RFPercentage } from "react-native-responsive-fontsize";

import { CustomText } from "./helpers/custom_text";
import { WeatherIcon } from "./helpers/icons";

const { height } = Dimensions.get("window");

export default function ListItem({
  name,
  temp,
  icon,
  latitude,
  longitude,
  getWeather,
}) {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => getWeather(latitude, longitude, false, true)}
    >
      <View style={[styles.container_city]}>
        <CustomText style={styles.city_text}>{name}</CustomText>
      </View>

      <View style={[styles.container_icon, styles.center]}>
        {WeatherIcon(icon, 5, "#CCCCCC")}
      </View>
      <View style={[styles.container_temp, styles.center]}>
        <CustomText style={styles.temp_text}>{Math.round(temp)}°</CustomText>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  //? ==========  GENERAL  ==========
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  evenly: {
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  between: {
    alignItems: "center",
    justifyContent: "space-between",
  },
  //? ==========  MAIN CONTAINER  ==========
  container: {
    width: "98%",
    marginLeft: "1%",
    height: height / 10,
    flexDirection: "row",
    backgroundColor: "rgba(255,255,255,0.1)",
    marginBottom: height / 50,
  },
  //? ==========  CONTAINER IN  ==========
  container_city: {
    height: "100%",
    width: "55%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: 10,
  },
  city_text: {
    color: "#39CCCC",
    fontSize: RFPercentage(3.9),
  },
  container_temp: {
    height: "100%",
    width: "25%",
  },
  temp_text: {
    color: "#39CCCC",
    fontSize: RFPercentage(5),
    paddingLeft: 15,
  },
  container_icon: {
    height: "100%",
    width: "20%",
  },
});
