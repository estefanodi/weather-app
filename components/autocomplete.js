import React from "react";
import { Dimensions } from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import { GOOGLE_API_KEY } from "../config";
const { width, height } = Dimensions.get("window");

export default function GooglePlacesInput({ getWeather, toggleInput }) {
  return (
    <GooglePlacesAutocomplete
      placeholder="Search"
      minLength={2}
      autoFocus={true}
      returnKeyType={"search"}
      listViewDisplayed={true}
      fetchDetails={true}
      onPress={async (data, details = null) => {
        const resJson = await fetch(
          `https://maps.googleapis.com/maps/api/geocode/json?place_id=${data.place_id}&key=${GOOGLE_API_KEY}`
        );
        const response = await resJson.json();
        const {lat,lng} = response.results[0].geometry.location
        getWeather(lat,lng,true)
      }}
      getDefaultValue={() => ""}
      query={{
        key: GOOGLE_API_KEY,
        language: "en", // language of the results
        types: "(cities)", // default: 'geocode'
      }}
      styles={{
        textInputContainer: {
          width,
          backgroundColor: "transparent",
          height: 100,
          alignItems: "center",
          justifyContent: "center",
        },
        textInput: {
          width: "90%",
          height: 50,
          borderWidth: 0.7,
          borderColor: "rgba(0,0,0,0.7)",
          fontSize: 20,
        },
        listView: {
          height: height / 3.2,
          backgroundColor: "rgba(0,0,0,0.1)",
        },
        description: {
          fontWeight: "bold",
          color: "#ffffff",
        },
        predefinedPlacesDescription: {
          color: "red",
        },
      }}
      currentLocation={false}
      currentLocationLabel="Current location"
      nearbyPlacesAPI="GooglePlacesSearch"
    />
  );
}
