import React from "react";
import { StyleSheet, Dimensions, View } from "react-native";
import { RFPercentage } from "react-native-responsive-fontsize";
import ListItem from "./fav_list_item";

const { width } = Dimensions.get("window");

export default function FavoritesContainer({
  favourites = [],
  deleteFavorite,
  getWeather,
}) {
  return (
    <View style={styles.container}>
      {favourites.map((ele) => {
        return (
          <ListItem
            key={ele._id}
            {...ele}
            deleteFavorite={deleteFavorite}
            getWeather={getWeather}
          />
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  //? ==========  GENERAL  ==========
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  evenly: {
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  between: {
    alignItems: "center",
    justifyContent: "space-between",
  },
  //? ==========  MAIN CONTAINER  ==========
  container: {
    width,
    justifyContent: "space-evenly",
    flexDirection: "column",
  },
  //? ==========  CONTENT  ==========
  forecast_in: {
    width: width / 3.3,
    height: "100%",
    borderWidth: 0.4,
    borderColor: "white",
    flexDirection: "column",
    backgroundColor: "rgba(255,255,255,0.1)",
  },
  forecast_in_text: {
    color: "#39CCCC",
    fontSize: RFPercentage(2.5),
  },
  forecast_in_title: {
    height: "20%",
  },
  forecast_in_icon: {
    height: "55%",
  },
  forecast_in_temp: {
    height: "25%",
    flexDirection: "row",
  },
  forecast_in_text2: {
    color: "#ffffff",
    fontSize: RFPercentage(5),
  },
});
